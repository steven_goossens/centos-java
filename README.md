## CentOS Java Dockerfile

This repository contains a **Dockerfile** of [CentOS](http://www.centos.org/) with [Oracle Java](https://www.oracle.com/java) installed for [Docker](https://www.docker.com/)'s [automated build](https://registry.hub.docker.com/u/dockerfile/java/) published to the public [Docker Hub Registry](https://registry.hub.docker.com/).

### Base Docker Image

* [centos:centos7](https://registry.hub.docker.com/_/centos/)

### Docker Tags

`stevengssns/centos-java` provides multiple tagged images:

* `latest` (default): AEM 6.0 (alias to `oracle-8-jre`)
* `oracle-7-jre`: Oracle Java version 7 JRE
* `oracle-7-jdk`: Oracle Java version 7 JDK
* `oracle-8-jre`: Oracle Java version 8 JRE
* `oracle-8-jdk`: Oracle Java version 8 JDK

### Installation

1. Install [Docker](https://www.docker.com/).

2. Download [automated build](https://registry.hub.docker.com/u/stevengssns/aem/) from public [Docker Hub Registry](https://registry.hub.docker.com/): `docker pull stevengssns/centos-java`

   (alternatively, you can build an image from Dockerfile: `docker build -t="stevengssns/centos-java" bitbucket.com/steven_goossens/centos-java`)

### Usage

    docker run -it stevengssns/centos-java:oracle-8-jdk
